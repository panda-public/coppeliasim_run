# coppeliasim_run

For from [https://github.com/jonasspring/coppeliasim_run/tree/8307c4c898fc02ad025c4af8d8b7d09780333c18](https://github.com/jonasspring/coppeliasim_run/tree/8307c4c898fc02ad025c4af8d8b7d09780333c18)

## Usage
This package runs coppeliasim _within_ a ROS node, so you can run it from a launch file.
